let fs = require("fs");
let input = fs.readFileSync("stdin", "utf8");
let lines = input.split("\n");

let qnt = parseInt(lines.shift());
let usuarios = [];
for(i = 0; i < qnt; i++) {
    let nome = lines.shift().split(" ");
    let nasc = lines.shift().split("/");
    let usuario = {
        nome: {
            primeiro: nome.shift().trim(),
            ultimo: nome.shift().trim()
        },
        nasc: {
            dia: parseInt(nasc.shift()),
            mes: parseInt(nasc.shift()),
            ano: parseInt(nasc.shift())
        },
        email: lines.shift().trim(),
        genero: lines.shift().trim(),
        pais: lines.shift().trim(),
        idade: function(){
            let data = new Date();
            if(this.nasc.mes == (data.getMonth()+1) && this.nasc.dia <= data.getDate()) {
                return data.getFullYear() - this.nasc.ano;
            } else if (this.nasc.mes < (data.getMonth()+1)) {
                return data.getFullYear() - this.nasc.ano;
            } else {
                return data.getFullYear() - this.nasc.ano - 1;
            }
        }
    }
    usuarios.push(usuario);
}

salvarArquivo(usuarios);

function salvarArquivo(arquivo) {
    let usuariosJSON = JSON.stringify(arquivo);
    fs.writeFileSync("Usuários", usuariosJSON);
}

let objetos = JSON.parse(fs.readFileSync("Usuários", "utf8"));
console.log(objetos);