package br.edu.ifpb.esp.poo2020.maratonistas;

import java.util.ArrayList;

public class Serie {
	
	public Integer ano = 2013;
	public ArrayList<Temporada> temporadas;
	public String titulo = "Brooklyn 99";
	public Double classificacao = 4.8;
	public Integer faixaEtaria = 14;
	public ArrayList<String> premios;
	public String sinopse = "Brooklyn 99 conta o dia a dia de uma delegacia de pol�cia no distrito do Brooklyn, Nova York. O maior foco s�o os detetives, que acabam mudando seu cotidiano quando o severo novo Capit�o Raymond Holt chega.";
	public String capa;
	public String genero = "Com�dia";
	
}