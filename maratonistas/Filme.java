package br.edu.ifpb.esp.poo2020.maratonistas;

import java.util.ArrayList;

public class Filme {
	
	public Integer ano = 2019;
	public String titulo = "Parasite";
	public Double classificacao = 4.9;
	public Integer faixaEtaria = 16;
	public ArrayList<String> premios;
	public String sinopse = "Toda a fam�lia de Ki-taek est� desempregada, vivendo em um por�o sujo e apertado, mas uma obra do acaso faz com que ele comece a dar aulas de ingl�s a uma garota de fam�lia rica. Fascinados com a vida luxuosa destas pessoas, pai, m�e e filhos bolam um plano para se infiltrarem tamb�m na fam�lia burguesa, um a um. No entanto, os segredos e mentiras necess�rios � ascens�o social custam caro a todos.";
	public String capa;
	public String genero = "Suspense";
	public String diretor = "Boong Joon-ho";
	public String autor;
	public String produtora;
	public String atores = "Cho Yeo-jeong\n" + "Park So-dam\n" + "Choi Woo-shik\n" + "Song Kang-ho";
	public String chat = "caralho oq foi isso";
	public String legenda = "Jessica, filha �nica, Illinois, Chicago";
	public String audio;
	public Integer duracao = 132;

}