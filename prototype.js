class Pessoa {
    constructor(nome, idade){
        this.nome = nome;
        this.idade = idade;
    }
} 

let p1 = new Pessoa('Luísa Ledra', 16);
console.log(p1.__proto__ == Pessoa.prototype); // retorna TRUE

let p2 = { nome: 'Luciana', sobrenome: 'Ledra' };
console.log(p2.__proto__ == Object.prototype); // retorna TRUE
console.log(Object.keys(p2)); // retorna uma lista com os nomes dos atributos (chaves) de p2: [ 'nome', 'sobrenome' ]
console.log(Object.values(p2)); // retorna uma lista com os valores dos atributos (chaves) de p2: [ 'Luciana', 'Ledra' ]
Object.freeze(p2); // congela o objeto p2, não permitindo mais alterações nos seus valores.

function apresentacao() {
    console.log(`Olá, meu nome é ${p1.nome} e eu tenho ${p1.idade} anos.`);
}
console.log(apresentacao.__proto__ == Function.prototype); // retorna TRUE
console.log(Function.prototype.__proto__ == Object.prototype); // retorna TRUE
console.log(apresentacao.prototype.__proto__ == Object.prototype); // retorna TRUE