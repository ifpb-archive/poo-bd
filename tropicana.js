class Pessoa {
    constructor(id, nome, telefone, endereco_bairro, endereco_rua, endereco_numero){
        this.id = id;
        this.nome = nome;
        this.telefone = telefone;
        this.endereco = {
            bairro = endereco_bairro, 
            rua = endereco_rua, 
            numero = endereco_numero
        };
    }
}

class Cliente extends Pessoa {
    constructor(id, nome, endereco_bairro, endereco_rua, endereco_numero){
        super(id, nome, endereco_bairro, endereco_rua, endereco_numero);
    }
}

class Entregador extends Pessoa {
    constructor(id, nome, telefone, salario){
        super(id, nome, telefone);
        this.salario = salario;
    }
}

class Ingrediente {
    constructor(id, nome, medida, valor, qnt_estoque){
        this.id = id;
        this.nome = nome;
        this.medida = medida;
        this.valor = valor;
        this.qnt_estoque = qnt_estoque;
    }
}

class Pizza {
    constructor(id, sabor, tamanho, valor){
        this.id = id;
        this.sabor = sabor;
        this.tamanho = tamanho;
        this.valor = valor;
    }
}

class Pedido {
    constructor(id, refri_marca, refri_tamanho, refri_qnt, refri_valor){
        this.id = id;
        this.refri_marca = refri_marca;
        this.refrigerante = {
            tamanho = refri_tamanho,
            qnt = refri_qnt,
            valor = refri_valor
        };
    }
    valor(){
        
    }
}