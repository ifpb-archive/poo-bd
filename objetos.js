//OBJETO LITERAL
let pessoa = {
    nome: {
        primeiro: 'Luísa',
        ultimo: 'Ledra'
    },
    idade: 16,
    sexo: 'feminino',
    interesse: ['editar', 'escutar música'],
    biografia: function() {
    console.log(`${this.nome.primeiro} ${this.nome.ultimo} tem ${this.idade} anos.`);
    console.log(`Ela gosta de ${this.interesse[0]} e ${this.interesse[1]}.`);
    }
}

//ACESSAR, ATUALIZAR, CRIAR E DELETAR
pessoa.nome.primeiro;
pessoa['nome']['ultimo'] = 'Azevedo';
pessoa.idade = 17;
pessoa['sexo'];
pessoa.interesse[0] = 'editar vídeos';
pessoa.interesse[1];
pessoa.biografia();
pessoa['altura'] = '1.53m';
pessoa.saudacao = function() { console.log('Olá a todos!'); }
delete pessoa['nome']['primeiro'];
delete pessoa.saudacao;

//FUNÇÃO CONSTRUTORA
function Nome(primeiro, ultimo) {
    this.primeiro = primeiro;
    this.ultimo = ultimo;
}
let nome1 = new Nome('Luísa', 'Ledra');
let nome2 = new Nome('Arthur', 'Ledra');

function Pessoa(nome, idade, sexo, interesse) {
    this.nome = nome;
    this.idade = idade;
    this.sexo = sexo;
    this.interesse = interesse;
    this.biografia = function() {
        console.log(`${this.nome.primeiro} ${this.nome.ultimo} tem ${this.idade} anos.`);
        this.sexo == 'feminino'? console.log(`Ela gosta de ${this.interesse[0]} e ${this.interesse[1]}.`)
        : console.log(`Ele gosta de ${this.interesse[0]} e ${this.interesse[1]}.`);
    }
}
let pessoa1 = new Pessoa(nome1, 16, 'feminino', ['editar vídeos', 'escutar música']);
let pessoa2 = new Pessoa(nome2, 14, 'masculino', ['ler mangá', 'assistir anime']);
pessoa1.biografia();
pessoa2.biografia();

//OBJECT.CREATE()
let CreatePessoa = {
    nome: {
        primeiro: 'Luísa',
        ultimo: 'Ledra'
    },
    idade: 16,
    sexo: 'feminino',
    interesse: ['editar vídeos', 'escutar música'],
    biografia: function(){
        console.log(`${this.nome.primeiro} ${this.nome.ultimo} tem ${this.idade} anos.`);
        console.log(`Ela gosta de ${this.interesse[0]} e ${this.interesse[1]}.`);
    }
}

let pessoa3 = Object.create(CreatePessoa);
pessoa3.biografia();

//ITERAÇÃO SOBRE PROPRIEDADES
for(i in CreatePessoa){
    console.log(`pessoa.${i} = ${CreatePessoa[i]}`);
}