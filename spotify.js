class Usuario {
    constructor(primeiro, ultimo, dia, mes, ano, email, senha, genero, pais) {
        this.nome = {
            primeiro: primeiro,
            ultimo: ultimo,
        }    
        this.nasc = {
            dia: dia,
            mes: mes,
            ano: ano,
        }
        this.email = email;
        this.senha = senha;
        this.genero = genero;
        this.pais = pais;
    } idade() {
        let data = new Date();
        if(this.nasc.mes == (data.getMonth()+1) && this.nasc.dia <= data.getDate()) {
            return data.getFullYear() - this.nasc.ano;
        } else if (this.nasc.mes < (data.getMonth()+1)) {
            return data.getFullYear() - this.nasc.ano;
        } else {
            return data.getFullYear() - this.nasc.ano - 1;
        }
    }
}

class Podcast {
    constructor(id, titulo, sobre, criador, ano) {
        this.id = id;
        this.titulo = titulo;
        this.sobre = sobre;
        this.criador = criador;
        this.ano = ano;
    }
}

class Episodio {
    constructor(id, titulo, duracao, descricao, dia, mes, ano, podcast) {
        this.id = id;
        this.titulo = titulo;
        this.duracao = duracao;
        this.descricao = descricao;
        this.data = {
            dia: dia,
            mes: mes,
            ano: ano,
        }
        this.podcast = podcast;
    }
}

class Playlist {
    constructor(id, nome, criador, privacidade) {
        this.id = id;
        this.nome = nome;
        if(criador) {
            this.criador = criador;
            this.privacidade = privacidade;
        }
        else {
            this.criador = "Spotify";
            this.privacidade = "pública";
        }
    }
}

class Musica {
    constructor(id, titulo, ano, minutos, segundos) {
        this.id = id;
        this.titulo = titulo;
        this.ano = ano;
        this.duracao = {
            minutos: minutos,
            segundos: segundos,
        }
    }
}

class Album {
    constructor(id, titulo, ano, faixas) {
        this.id = id;
        this.titulo = titulo;
        this.ano = ano;
        this.faixas = faixas;
    }
}

class Artista {
    constructor(id, primeiro, ultimo, sobre, discografia) {
        this.id = id;
        this.nome = {
            primeiro: primeiro,
            ultimo: ultimo,
        }
        this.sobre = sobre;
        this.discografia = discografia;
    }
}

let usuario1 = new Usuario("Luísa", "Ledra", 31, 07, 2004, "luisaledra@gmail.com", "luisa123", "Feminino", "Brasil");
let podcast1 = new Podcast(1, "Músicas Zen", "Músicas para relaxar", usuario1, 2020);
let episodio1 = new Episodio(1, "Sons de chuva e piano", "Música para meditação com som de chuva e piano", 65, 10, 11, 2020, podcast1);
let playlist1 = new Playlist(1, "Nando Reis Rádio");
let musica1 = new Musica(1, "Relicário (Ao Vivo)", 2015, 5, 47);
let musica2 = new Musica(2, "Por Onde Andei (Ao Vivo)", 2015, 4, 49);
let album1 = new Album(1, "Voz e Violão", 2015, [musica1, musica2]);
let artista1 = new Artista(1, "Nando", "Reis", "Cantor, compositor, multi-instrumentista e produtor musical brasileiro que fez parte da banda Titãs", [album1]);