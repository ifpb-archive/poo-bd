class Pessoa {
    constructor(id, nome, anoNascimento, anoMorte) {
        this.id= id;
        this.nome = nome;
        this.anoNascimento = anoNascimento;
        this.anoMorte = anoMorte;
    }
}

module.exports = Pessoa;