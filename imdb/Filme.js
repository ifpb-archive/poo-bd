class Filme {
    constructor(id, titulo, nota, numVotos, ano, duracao, genero, diretores, escritores, elenco) {
        this.id = id;
        this.titulo = titulo;
        this.nota = nota;
        this.numVotos = numVotos;
        this.ano = ano;
        this.duracao = duracao;
        this.genero = genero;
        this.diretores = diretores;
        this.escritores = escritores;
        this.elenco = elenco;
    }
}

module.exports = Filme;