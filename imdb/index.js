const fs = require("fs");
const Pessoa = require("./Pessoa");
const Filme = require("./Filme");

let inputNomes = fs.readFileSync("./imdb/pessoas.tsv", "utf8");
let nomes = inputNomes.split("\n");
nomes.shift();
let pessoas = [];
for(let linha of nomes) {
    let [id, nome, anoNascimento, anoMorte] = linha.split("\t");
    let pessoa = new Pessoa(id, nome, Number(anoNascimento), Number(anoMorte));
    pessoas.push(pessoa);
}

let inputFilmes = JSON.parse(fs.readFileSync("./imdb/filmes.json", "utf8"));
let filmes = [];
for(f in inputFilmes) {
    let {id, titulo, nota, numVotos, ano, duracao, genero, diretores, escritores, elenco} = inputFilmes[f];
    let filme = new Filme(id, titulo, Number(nota), Number(numVotos), ano, duracao, genero, diretores, escritores, elenco);
    filmes.push(filme);
}

function atorMaisFilmes(){
    let qnt = [];
    for(filme of filmes){
        if(filme.elenco){
            for(pessoa of filme.elenco){
                if(pessoa.categoria == "actor" || pessoa.categoria == "actress") {
                    let id = pessoa.idPessoa;
                    qnt[id]? qnt[id]++ : qnt[id] = 1;
                }
            }
        }
    }
    let maior = 0;
    let idMaior;
    for(chave in qnt){
        if(qnt[chave] > maior){
            idMaior = chave;
        }
    }
    console.log(pessoas.find((pessoa) => pessoa.id == idMaior).nome);
}

function maioresNotas(){
    let notas = filmes.filter((filme) => filme.numVotos > 5000);
    notas.sort((a, b) => b.nota - a.nota);
    for(i = 0; i < 10; i++){
        console.log(notas[i].titulo, notas[i].nota);
    }
}

function filmesDoAtor(nome){
    let id = pessoas.find((pessoa) => pessoa.nome == nome).id;
    let titulos = [];
    for(filme of filmes){
        if(filme.elenco){
            for(ator of filme.elenco){
                if(ator.idPessoa == id){
                    titulos.push(filme.titulo);
                }
            }
        }
    } console.log(titulos);
}

atorMaisFilmes();
maioresNotas();
filmesDoAtor("Emma Watson");